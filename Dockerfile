FROM alpine:latest

EXPOSE 8888/tcp

RUN apk add tinyproxy
COPY ./tinyproxy.conf /etc/tinyproxy/.

ENTRYPOINT ["/usr/bin/tinyproxy", "-d"]
