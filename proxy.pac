function FindProxyForURL(url, host) {
  const domains = [
    "tinyproxy.stats",
    "tutanota.com",
    "protonmail.com",
    "ngrok.com",
    "squarespace.com",
    "webhookrelay.com",
    "syncthing.net",
  ];

  const domainCheck = (domain) => dnsDomainIs(host, domain);

  if (domains.some(domainCheck)) {
    return "PROXY 192.168.1.205:8080";
  }
}
